package controleur;

import java.awt.event.ActionEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import modele.Plateau;
import modele.PropagationConfigurer;
import modele.PropagationJeu;
import vues.Grille;
import vues.Menu;

/**
 * 
 * Pour cette classe nous avons besoin des modeles present dans le package
 * modele, les vues du package vues ainsi que les methodes de la classe
 * ActionEvent Classe du controleur du menu, servant a gerer l appui des
 * boutons du jeu eteindre la lumiere
 * 
 * @author Charlie Kieffer Theo Chapelle
 */

public class ControleurMenu {

    /**
     * Un Menu menu, un JPanel contenant tout les boutons et le nombre de
     * deplacements realises lors du jeu
     */
    private Menu menu;

    /**
     * Une Grille grille, un JPanel contenant toutes les lumieres
     */
    private Grille grille;

    /**
     * constante qui sert a definir le nombre de lumieres allume si on clique sur le bouton "Aleatoire"
     */
    private static final int NB_ALEA = 8;

    /**
     * Constructeur de la classe ControleurMenu
     * 
     * @param g la grille pour construire le controleur
     * @param m le menu pour construire le controleur
     */
    public ControleurMenu(Grille g, Menu m) {
        this.menu = m;
        this.grille = g;

        this.menu.getJouer().setEnabled(false);
        this.menu.getSauvegarder().setEnabled(false);

        /**
         * Ajout d un event lors de l appuie du bouton configurer celui ci permet de
         * passer en mode de propagation configurer afin de choisir les cases qui seront
         * allumes de base il sert aussi a desactiver le bouton aleatoire
         */
        this.menu.getConfigurer().addActionListener((ActionEvent e) -> {
            Plateau p = this.grille.getPlateau();
            p.setPropagation(new PropagationConfigurer(p));

            menu.getJouer().setEnabled(true);
        });

        /**
         * Ajout d un event lors de l appuie du bouton aleatoire celui ci permet d
         * allumer aleatoirement des lumieres de la grille on parcourt la grille et les
         * lumieres ont une chance de s allumer tant que le nombre de lumieres est
         * differents de la constante NB_ALEA on continue cela desactive alors le bouton
         * configurer
         */
        this.menu.getAleatoire().addActionListener((ActionEvent e) -> {
            int index = 0;
            for (int i = 0; i < Plateau.NB_LUMIERES_PAR_COLONNE; i++) {
                for (int j = 0; j < Plateau.NB_LUMIERES_PAR_LIGNE; j++) {
                    grille.getPlateau().getLumieres()[i][j].setAllume(false);
                }
            }
            while (index < NB_ALEA) {
                int x = (int) Math.floor(Math.random() * Plateau.NB_LUMIERES_PAR_COLONNE);
                int y = (int) Math.floor(Math.random() * Plateau.NB_LUMIERES_PAR_LIGNE);
                if (!grille.getPlateau().getLumieres()[x][y].getAllume()) {
                    grille.getPlateau().getLumieres()[x][y].inverser();
                    index++;
                }
            }

            menu.getJouer().setEnabled(true);
        });

        /**
         * Ajout d un event lors de l appuie du bouton quitter il permet d eteindre
         * toute les lampes, de renitialiser le nombre de deplacement ainsi que de
         * reactiver tout les boutons qui ont ete potentiellement desactive
         */
        this.menu.getQuitter().addActionListener((ActionEvent e) -> {
            for (int i = 0; i < Plateau.NB_LUMIERES_PAR_COLONNE; i++) {
                for (int j = 0; j < Plateau.NB_LUMIERES_PAR_LIGNE; j++) {
                    grille.getPlateau().getLumieres()[i][j].setAllume(false);
                }
            }
            this.grille.getPlateau().setDeplacements(0);
            menu.getValeurDep().setText("" + grille.getPlateau().getDeplacements());

            menu.getConfigurer().setEnabled(true);
            menu.getAleatoire().setEnabled(true);
            this.menu.getJouer().setEnabled(false);
            this.menu.getSauvegarder().setEnabled(false);

            grille.getPlateau().setPropagation((int x, int y) -> {
            });
        });

        /**
         * Ajout d un event lors de l appuie du bouton jouer il permet de passer en mode
         * de propagation Jeu il desactive egalement les boutons jeu configurer et
         * aleatoire
         */
        this.menu.getJouer().addActionListener((ActionEvent e) -> {
            Plateau p = this.grille.getPlateau();
            p.setPropagation(new PropagationJeu(p));
            menu.getConfigurer().setEnabled(false);
            menu.getAleatoire().setEnabled(false);
            menu.getJouer().setEnabled(false);
            menu.getSauvegarder().setEnabled(true);
        });

        /**
         * Ajout d un event lors de l appuie du bouton sauvegarder il permet de
         * sauvegarder le plateau actuel
         */
        this.menu.getSauvegarder().addActionListener((ActionEvent e) -> {
            ObjectOutputStream ecriture = null;
            try {
                ecriture = new ObjectOutputStream(new FileOutputStream("../resources/save.txt"));
                ecriture.writeObject(this.grille.getPlateau());
                ecriture.close();
            } catch (FileNotFoundException e1) {
                System.out.println("Le fichier de sauvegarde n'a pas été trouvé");
            } catch (IOException e1) {
                System.out.println("Il y a eu une erreur dans la sauvegarde");
            }
        });

        /**
         * Ajout d un event lors de l appuie du bouton recharger : il permet de
         * recharger le plateau précédemment sauvegardé
         */
        this.menu.getRecharger().addActionListener((ActionEvent e) -> {
            ObjectInputStream lecture = null;
            try {
                lecture = new ObjectInputStream(new FileInputStream("../resources/save.txt"));
                Plateau plateau = (Plateau) lecture.readObject();
                if (plateau.getLumieres().length > Plateau.NB_LUMIERES_PAR_COLONNE || plateau.getLumieres()[0].length > Plateau.NB_LUMIERES_PAR_LIGNE) {
                    System.out.println("La sauvegarde que vous essayez de charger contient un plateau trop grand par rapport à celui actuel !");
                }
                else {
                    for (int i = 0; i < plateau.getLumieres().length; i++) {
                        for (int j = 0; j < plateau.getLumieres()[0].length; j++) {
                            this.grille.getPlateau().getLumieres()[i][j].setAllume(plateau.getLumieres()[i][j].getAllume());
                        }
                    }
                    this.grille.repaint();
                    this.grille.getPlateau().setDeplacements(plateau.getDeplacements());
                    menu.getValeurDep().setText("" + plateau.getDeplacements());
                    Plateau p = this.grille.getPlateau();
                    p.setPropagation(new PropagationJeu(p));
                    menu.getConfigurer().setEnabled(false);
                    menu.getAleatoire().setEnabled(false);
                    menu.getJouer().setEnabled(false);
                    menu.getSauvegarder().setEnabled(true);
                }
                
                lecture.close();
                
            } catch (FileNotFoundException e1) {
                System.out.println("Le fichier de sauvegarde n'a pas été trouvé");
            } catch (IOException e1) {
                System.out.println("Il y a eu une erreur dans le chargement du plateau");
            } catch (ClassNotFoundException e1) {
                System.out.println("Le fichier de sauvegarde a probablement été corrompu");
            }
        });
    }
}