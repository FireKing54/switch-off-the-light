package modele;

import java.io.Serializable;

/**
 * Classe modélisant la propagation en mode Configurer
 */
public class PropagationConfigurer implements Propagation, Serializable {
    
    /**
     * Lien vers le plateau actuel
     */
    private Plateau plateau;

    /**
     * Constructeur qui construit la propagation avec le plateau donné
     * @param p plateau donné
     */
    public PropagationConfigurer(Plateau p) {
        this.plateau = p;
    }

    /**
     * Méthode de propagation en mode Configurer
     * @param x coordonnée x de la lumière
     * @param y coordonnée y de la lumière
     */
    @Override
	public void propager(int x, int y) {
        this.plateau.selectionner(x, y);
	}

}