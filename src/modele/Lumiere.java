package modele;

import java.io.Serializable;
import java.util.Observable;

/**
 * Classe qui modélise une lumière du jeu
 */
public class Lumiere extends Observable implements Serializable {

    /**
     * Est à true si la lumiere est allumée
     */
    private boolean allume;
    
    /**
     * Constructeur qui initialise la lumière
     */
    public Lumiere(){
        this.allume = false;
    }

    /**
     * Méthode qui inverse la lumière
     */
    public void inverser(){
        this.allume = (!this.allume);
        notifier();
    }

    /**
     * getter de la lumlière
     * @return la lumière
     */
    public boolean getAllume(){
        return this.allume;
    }

    /**
     * Méthode qui notifie les observeurs d'un changement
     */
    public void notifier() {
        setChanged();
        notifyObservers();
    }

    /**
     * Setter de la lumière
     * @param boolean nouvel état de la lumière
     */
    public void setAllume(boolean oui){
        this.allume = oui;
        notifier();
    }
    

}