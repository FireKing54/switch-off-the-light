package modele;

/**
 * Interface symbolisant la propagation
 */
@FunctionalInterface
public interface Propagation {
    public void propager(int x, int y);
}