package modele;

import java.io.Serializable;

import vues.Grille;

/**
 * Classe représentant un plateau de jeu
 */
public class Plateau implements Serializable {
    
    /**
     * Lumières du plateau
     */
    private Lumiere[][] lumieres;

    /**
     * Objet contenant la méthode de propagation en fontion du mode de jeu
     */
    private Propagation propagation;

    /**
     * Nombre de déplacements
     */
    private int nbDep;

    /**
     * Constante du nombre de lumières par ligne
     */
    public static final int NB_LUMIERES_PAR_LIGNE = 7;

    /**
     * Constante du nombre de lumières par colonne
     */
    public static final int NB_LUMIERES_PAR_COLONNE = 7;

    /**
     * Constructeur qui construit un plateau standard
     */
    public Plateau() {
        this.nbDep = 0;
        this.lumieres = new Lumiere[NB_LUMIERES_PAR_COLONNE][NB_LUMIERES_PAR_LIGNE];
        // Initialise la méthode de propagation, de telle sorte à ce qu'il n'y ait pas de propagation
        this.propagation = (int x, int y) -> {};
        for(int i = 0 ; i < NB_LUMIERES_PAR_COLONNE ; i++) {
            for(int j = 0 ; j < NB_LUMIERES_PAR_LIGNE ; j++) {
                this.lumieres[i][j] = new Lumiere();
            }
        }
    }

    /**
     * Méthode qui inverse l'état d'une lampe aux coordonnées données
     * @param x coordonnée x de la lampe
     * @param y coordonnée y de la lampe
     */
    public void selectionner(int x, int y) {
        this.lumieres[x][y].inverser();
    }

    /**
     * Méthode de propagation en fonction du mode de jeu
     * @param x coordonnée x de la lampe
     * @param y coordonnée y de la lampe
     */
    public void propager(int x, int y) {
        propagation.propager(x, y);
    }

    /**
     * Getter de lumières
     * @return les lumières
     */
    public Lumiere[][] getLumieres() {
        return this.lumieres;
    }

    /**
     * Setter de la propagation
     * @param p propagation
     */
    public void setPropagation(Propagation p) {
        this.propagation = p;
    }

    /**
     * Getter de la propagation
     * @return la propagation courante
     */
    public Propagation getPropagation() {
        return this.propagation;
    }

    /**
     * Méthode qui renvoie true si le joueur a gagné
     * @return true
     */
    public boolean gagner() {
        Lumiere[][] lumieres = this.getLumieres();
        // Le joueur ne gagne pas si il n'est pas en mode de jeu
        if (! (this.propagation instanceof PropagationJeu)) {
            return false;
        }
        for (int i = 0; i < NB_LUMIERES_PAR_COLONNE; i++) {
            for (int j = 0; j < NB_LUMIERES_PAR_LIGNE; j++) {
                if (lumieres[i][j].getAllume()) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Méthode toString
     * @return affichage formatté
     */
    public String toString() {
        StringBuffer res = new StringBuffer("Plateau \n");
        for (int i = 0; i < NB_LUMIERES_PAR_COLONNE; i++) {
            for (int j = 0; j < NB_LUMIERES_PAR_LIGNE; j++) {
                res.append((this.lumieres[i][j].getAllume()) ? "A" : "E");
            }
            res.append("\n");
        }
        return res.toString();
    }

    /**
     * Méthode qui incrémente de 1 le nombre de déplacements
     */
    public void incrementerDeplacement() {
        this.nbDep++;
    }

    /**
     * Getter du nombre de déplacements
     * @return nombre de déplacements
     */
    public int getDeplacements() {
        return this.nbDep;
    }

    /**
     * Setter du nombre de déplacements
     * @param nombre de déplacements
     */
    public void setDeplacements(int nbDep) {
        this.nbDep = nbDep;
    }

}